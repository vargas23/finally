﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clase1 : MonoBehaviour {


    // ciclo de vida
    // - nuestros scripts se acoplan a una logica centralizada que ya tiene un orden

    // se ejecuta al iniciar el objeto una sola vez
    void Awake () {
        print("AWAKE " + transform.name);
    }

    // se ejecuta una vez después de todos los awakes
	void Start () {
        Debug.Log("START");
	}
	
	// Update is called once per frame
	void Update () {
		// fps - frames per second
        // framerate - 30 fps
        // aplicación en tiempo real

	}

    void FixedUpdate() { 
    }

    void LateUpdate() { 
    
    }
}
